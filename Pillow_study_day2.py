"Name:Qingyuan Liu"
"Date: 13/10/2020"
"Learn the Pillow function like ImageEnhance, ImageOps," \
"and try to create something fancy"


import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps

def main():
    #open the image that going to use and make a smaller version of picture,
    # use IamgeOps to change it to gray.then show how it looks like.
    image1 = Image.open("/Users/rwkm/Documents/Python study/test1.jpg")
    imageSizeChange = image1.reduce(2)
    altered_picture = ImageOps.grayscale(imageSizeChange).convert("RGB")
    #altered_picture.show()



    width,height = altered_picture.size
    imageData = np.zeros((height,width), dtype= np.uint8)

    
    slice = imageData[(height // 4):(3 * height // 4), (width // 4):(3 * width // 4)]
    imageData[(height // 4):(3 * height // 4), (width // 4):(3 * width // 4)] = slice // 2
    picture = Image.fromarray( imageData )
    picture.show()


    imageData[:,width //2:] = 255
    mask = Image.fromarray( imageData, mode = "L" )
    half_and_half = Image.composite( imageSizeChange, altered_picture, mask )
    half_and_half.show()


if __name__ == "__main__":
    main()
